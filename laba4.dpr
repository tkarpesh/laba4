Program L4DinArr;
{$APPTYPE CONSOLE}
uses
  SysUtils;

var
  N: Integer; //The size of Matrix;
  i, j, k, d: Integer;
  Matrix: array of array of Integer;
  Mass: array of Integer;
begin
  Writeln('Enter N - ');
  Readln(N);
  SetLength(Matrix, N, N);
  SetLength(Mass, N * N);
  d := - 1;
  k := - 1;
  Writeln(' Matrix :');
  Randomize;
  For i := 0 to N - 1 do
  begin
    for j := 0 to N - 1 do
    begin
      Matrix[i, j] := Random(20) + 1;
      write(Matrix[i, j]:3);
    end;
    Writeln(#13#10);
  end;

  repeat
    d := d + 1;
    for i := d to (n - d - 1) do
    begin
      k := k + 1;
      Mass[k] := Matrix[d, i];
    end;

    for i := d + 1 to (n - d - 1) do
    begin
      k := k + 1;
      Mass[k] := Matrix[i, n - d - 1];
    end;

    for i := (n - d - 2) downto d do
    begin
      k := k + 1;
      Mass[k] := Matrix[(n - d - 1), i];
    end;

    for i := (n - d - 2) downto d + 1 do
    begin
      k := k + 1;
      Mass[k] := Matrix[i, d];
    end;
  until k = ((n * n) - 1);

  Writeln('Mass: ');
  for i := 0 to k do
  begin
    write(Mass[i]:3);
  end;
  Readln;
end.
